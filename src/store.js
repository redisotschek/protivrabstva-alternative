import Vue from 'vue'
import Vuex from 'vuex'
import axios from "axios";
import ApiAuthService from './services/auth.service'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    token: localStorage.getItem('user-token') || '',
    status: '',
  },
  getters: {
    isAuthenticated: state => !!state.token,
    authStatus: state => state.status,
    getToken: state => state.token
  },
  mutations: {
    ['AUTH_REQUEST']: (state) => {
      state.status = 'loading'
    },
    ['AUTH_SUCCESS']: (state, token) => {
      state.status = 'success'
      state.token = token
    },
    ['AUTH_ERROR']: (state) => {
      state.status = 'error'
    },
    ['AUTH_LOGOUT']: (state) => {
      state.status = 'unauthenticated'
      state.token = null
    },
  },
  actions: {
    ['AUTH_REQUEST']: ({commit, dispatch}, user) => {
      return new Promise((resolve, reject) => { // The Promise used for router redirect in login
        commit('AUTH_REQUEST')
        ApiAuthService.login(user.username, user.password).then(resp => {
          switch (resp.data) {
            case '': {
              commit('AUTH_ERROR', 'Wrong username or password')
              localStorage.removeItem('user-token') // if the request fails, remove any possible user token if possible
              reject('Wrong username or password')
              break
            }
            default: {
              const token = resp.data
              localStorage.setItem('user-token', token) // store the token in localstorage
              // axios.defaults.headers.common['Authorization'] = token
              commit('AUTH_SUCCESS', token)
              // you have your token, now log in your user :)
              // dispatch('USER_REQUEST')
              resolve(resp)
            }
          }
        })
        .catch(err => {
          console.log(err)
        })
      })
    },
    ['AUTH_LOGOUT']: ({commit, dispatch}) => {
      return new Promise((resolve, reject) => {
        commit('AUTH_LOGOUT')
        localStorage.removeItem('user-token') // clear your user's token from localstorage
        resolve()
      })
    }
  }
})
