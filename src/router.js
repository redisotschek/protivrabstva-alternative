import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import About from './views/About.vue'
import PrQuestions from './views/PrQuestions'
import Login from './views/Login'
import Admin from './views/Admin'
import News from './views/News'
import Help from './views/Help'
import NotReady from './views/NotReady'
import Sos from './views/Sos'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/about',
      name: 'about',
      component: About
    },
    {
      path: '/faq',
      name: 'faq',
      component: PrQuestions
    },
    {
      path: '/news',
      name: 'news',
      // component: News
      component: NotReady
    },
    {
      path: '/people',
      name: 'people',
      // component: People
      component: NotReady
    },
    {
      path: '/slavery',
      name: 'slavery',
      // component: Slavery
      component: NotReady
    },
    {
      path: '/login',
      name: 'login',
      component: Login
    },
    {
      path: '/admin',
      name: 'admin',
      component: Admin
    },
    {
      path: '/help',
      name: 'help',
      component: Help
    },
    {
      path: '/sos',
      name: 'sos',
      component: Sos
    },
    {
      path: '*',
      name: '404',
      redirect: '/'
    }
  ]
})
