import axios from 'axios'
import store from '../store.js'
import router from '../router'

export default class ApiNewsService {

  static addPost (post) {
    const params = new URLSearchParams();
    params.append('location', 'addNews');
    params.append('h', post.h);
    params.append('text', post.text);
    params.append('token', post.token);
    // params.append('pic', post.image);
    // console.error('Не забудь нормально отправить картинку');
    return axios.post(
      'ControlerNews.php',
      params
    )
  }

  static addPic (post) {
    const params = new URLSearchParams();
    params.append('pic', post.image);
    // console.error('Не забудь нормально отправить картинку');
    return axios.post(
      'upload.php',
      params,
      {
        headers: {
          'Content-Type': 'multipart/form-data'
        }
      }
    )
  }

  static getNews () {
    const params = new URLSearchParams();
    params.append('location', 'getAll');
    params.append('offset', 15);
    return axios.post(
      'ControlerNews.php',
      params
    )
  }
}